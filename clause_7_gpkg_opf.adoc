[[clause_7_gpkg_opf]]
== GeoPackage Open Portrayal Framework (Informative)
The goal of the OGC Open Portrayal Framework (OPF) is to allow production of high-quality digital maps over the web from existing vector data.
This section describes the challenges and proposed mitigations specifically for GeoPackage producers and clients.
<<opf_overview>> illustrates a generalized view of a spatial data infrastructure.
As shown on the left side of the diagram, features and styles are often developed independently of each other.
A data producer may not have any knowledge of the portrayal requirements which are often the purview of a completely different organization.
A successful OPF must also account for the possibility that there is a many-to-many relationship between potential data sources and potential visualizations.

[#opf_overview,reftext='{figure-caption} {counter:figure-num}']
.Open Portrayal Framework Overview
image::images/overview.png[]

The rest of this section explores the following three challenges to implementing an OPF:

. Conveying the styling rules;
. Coupling layers with styles;
. Making the styling rules accessible operationally.

=== Conveying Styling Rules
Styling rules must be encapsulated in a common encoding that can be used by GeoPackage clients to render (draw) the features onto the screen in a prescribed way.
Participants in the OGC Testbed-15 OPF Thread asserted that it is not realistic for the community to select a single encoding for styling rules.
There are too many diverse requirements, existing encodings are too entrenched, and introducing a completely new encoding would be too risky and expensive.
That said, the GeoPackage community would benefit from a default encoding.
An ideal encoding would be standardized, easy to implement and use, and capable of handling the gamut of basic portrayal needs.
Unfortunately, no single encoding has all three of these traits.
As part of the OPF thread in Testbed-15, participants are using two encodings.

* OGC's link:https://www.opengeospatial.org/standards/sld/[Styled Layer Descriptor (SLD)] is an OGC standard and handles most basic portrayal requirements, but its XML-based structure is considered by many to be difficult to use.
In addition, XML is a poor choice for mobile computing environments where GeoPackage is commonly used.
* link:https://docs.mapbox.com/mapbox-gl-js/style-spec/[Mapbox Styles] is not standardized but it does handle most basic portrayal requirements and its JSON-based structure is considered relatively easy to use.

As a result, the submitters recommend a flexible approach to portrayal encodings.

* Organizations that demand standards-based approaches should use SLD.
* Organizations with more flexible requirements may be better off using Mapbox Styles.
* GeoPackage should support other encodings, either explicitly or through the use of extensions as is proposed with tiled feature data (aka vector tiles)footnote:[The current proposal for tiled feature data is to have one extension to declare that vector tiles are allowed, then a separate extension to allow a `tiles` column to have a specific encoding such as Mapbox Vector Tiles or GeoJSON.].
This will allow organizations to innovate as technology evolves. 

While allowing multiple style encodings does not maximize interoperability, it is the best solution available at this time that considers the needs of both the procurement community and the developers operating in a more agile environment.
Each portrayal engine will have its own internal model for handling styles.
The emergence of a symbology conceptual model (currently under development as OGC 18-067) will assist developers in mapping from one or more style encodings to their internal model.

=== Coupling Layers and Styles
Since styles and data layers are often developed independently, an effective spatial data infrastructure needs a way to couple layers with styling rules that are appropriate for those layers.
This is particularly important in GeoPackage where software capabilities tend to be more limited to support streamlined workflows that are easy to train for.
The first requirement for effective style management is to provide a resolvable URI for each style.
After that, there is some flexibility.

* Independent map views, each with their own sets of layers and corresponding styles, can be encoded in the GeoPackage as OWS Context files using the proposed <<owscontext_extension,OWS Context Extension>>. 
For example, a topographic map may have no background (or possibly a single-colored base map as a background) and one or more feature layers, each with a specific style (referenced by its URI).
A corresponding satellite overlay map may have raster tiles as a background with the same set of feature layers, but with a different set of styles.
The GeoPackage client then provides a list of available contexts to the operator.
This approach is the simplest for the operator, but it does not provide the operator any way to customize the display.
* Semantic Annotations can be used to <<sa_layers_to_styles,couple layers with styles>>.
In this case, it is the GeoPackage client's responsibility to provide the appropriate list of styles to the operator.
The operator can then select the desired style on a layer-by-layer basis.
* Semantic Annotations can also be used to <<sa_layers_to_stylable_layer_sets, couple layers with stylable layer sets>>.
In this case, the GeoPackage client's and operator's responsibilities are similar, but the organization of the styles is slightly different.

=== Making Styles Accessible Operationally
While the previous section describes how to couple layers and styles together, it does not address the challenge of getting the styles to the GeoPackage client so that they can be used to visualize the data.
Since there are a number of operational settings that a GeoPackage client may operate in, the infrastructure needs a number of ways to retrieve style information.
Some of these ways include the following:

. In a connected environment, a GeoPackage client may retrieve a style directly via its URI.
In a mature architecture, the service hosting the style would be able to provide it in more than one encoding so that it can meet the needs of disparate clients.
. In a disconnected environment, a GeoPackage client may retrieve a style directly from a GeoPackage via the proposed <<styles_extension,Styles Extension>>.
When used in conjunction with <<machine_readable_manifests,manifests>>, a client would be able to discover that the Styles Extension is in use and what specific options (particularly encodings) are supported.
The responsibility of the GeoPackage producer would then be to ensure that the styles are added in all of the necessary encodings and that the manifest fully reflects the GeoPackage's contents.
. In a mixed environment, a GeoPackage client may retrieve a style through a style service (i.e., registry) that can resolve a style via its URI.
In this scenario, the service decouples the style from where it is storedfootnote:[In this scenario, the styles may be stored directly in the GeoPackage as per the previous item.].
This provides flexibility beyond what would be appropriate for a GeoPackage Client to manage by itself.
