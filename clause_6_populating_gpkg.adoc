== Populating GeoPackages with Vector Tiles

Here is what you need to do to populate your GeoPackage with vector tiles.
There are a bunch of options depending on how complex you want to make it and what client-side capabilities you want to support.

=== Vector Tiles

[[embedded_attributes]]
==== Embedded Attributes
In this approach, the attributes remain embedded in the vector tiles.
This approach requires the least amount of server-side processing to produce the GeoPackage.

. Ensure required tables are present:
** `gpkg_extensions`
** `gpkgext_vt_layers`
** `gpkgext_vt_fields`
. Populate `gpkg_extensions` with:
** references to all tables mentioned above
** reference to the MVT Extension and/or GeoJSON Extension as needed
. Add a row to `gpkg_contents` for your user-defined vector tiles table with a `data_type` of "vector-tiles".
. Add a row to `gpkgext_vt_layers` for each vector tiles layer, referencing the `table_name` from `gpkg_contents`.
. Add a row to `gpkgext_vt_fields` for each field referenced in the vector tiles, referencing the `layer_id` from `gpkgext_vt_layers`.
. Add a row to your user-defined vector tiles table for each vector tile.

[#img_attributes,reftext='{figure-caption} {counter:figure-num}']
.Vector Tiles with Embedded Attributes
image::images/vt.png[]

[[relational_attributes]]
==== Relational Attributes
This approach allows you to query on attributes without opening the vector tiles.
It also saves storage space by eliminating the redundancy of having attribute information duplicated across multiple tiles.
This approach works well with querying if you know which tiles will contain your results or there are few tiles in your area of interest.

. Ensure required tables are present:
** `gpkg_extensions`
** `gpkgext_vt_layers`
. Populate `gpkg_extensions` with:
** references to all tables mentioned above
** reference to the MVT Extension and/or GeoJSON Extension as needed
** reference to the Vector Tiles Attributes Extension
. Add a row to `gpkg_contents` for your user-defined vector tiles table and your user-defined attributes table.
. Modify the vector tiles to remove all embedded attributes before adding them to the user-defined vector tiles table.
. Add all attributes to the user-defined attributes table.
. Add a row to `gpkgext_vt_layers` for each vector tiles layer, with a reference to the user-defined attributes table containing the attributes.

[#img_attributes,reftext='{figure-caption} {counter:figure-num}']
.Vector Tiles with Relational Attributes
image::images/attributes.png[]

==== Mapped Relational Attributes
This approach allows you to perform a query and identify the tiles that contain matching results.
The following are in addition to <<relational_attributes>>:

. Ensure required tables are present:
** `gpkgext_relations`
. Populate `gpkg_extensions` with:
** references to all tables mentioned above
** reference to the Related Tables Extension
. Establish a related tables relationship between the vector tiles layers and features.
.. Add a row to `gpkg_contents` for your user-defined mapping table.
.. Add a row to `gpkgext_relatons` for a related tables relationship between the user-defined vector tiles table and the user-defined attributes table via the mapping table
.. Add a row to the mapping table for each tile (by tile ID) and each feature (by feature ID)  

[#img_attributes,reftext='{figure-caption} {counter:figure-num}']
.Vector Tiles with Mapped Attributes
image::images/attributes_mapped.png[]

=== Styles and Symbols
This section extends the previous section to provide the information needed to render the vector tiles properly.

[[simple_styles]]
==== Simple Styles and Symbols
In this approach, the client must couple the layers to the stylesheets.

. Ensure required tables are present:
** `gpkgext_stylesheets`
** `gpkgext_symbols`
** `gpkgext_symbol_content`
. Populate `gpkg_extensions` with:
** references to all tables mentioned above
. Add a row to `gpkgext_stylesheets` for each stylesheet capable of rendering the layer.
. Add a row to `gpkgext_symbol_content` for each file containing symbol data.
. Add a row to `gpkgext_symbols` for each symbol used in your stylesheets, referencing a row in `gpkgext_symbox_contents` and, if needed, sprite information.

[#img_styles,reftext='{figure-caption} {counter:figure-num}']
.Styles with Vector Tiles
image::images/vtp.png[]

[[coupling_with_semantic_annotations]]
==== Coupling Layers and Stylesheets with Semantic Annotations
This approach uses semantic annotations to couple layers and stylesheets.
The following are in addition to <<simple_styles>>:

. Ensure required tables are present:
** `gpkgext_relations`
** `gpkgext_semantic_annotations`
. Populate `gpkg_extensions` with:
** references to all tables mentioned above
** reference to the Related Tables Extension
. Add a row to `gpkgext_semantic_annotations` describing the URI for the stylesheet
. Establish a related tables relationship between the vector tiles layers and semantic annotations.
.. Add a row to `gpkg_contents` for your user-defined mapping table.
.. Add a row to `gpkgext_relatons` for a related tables relationship between the user-defined vector tiles table and `gpkgext_semantic_annotations` via the user-defined mapping table
.. Add a row to the mapping table for each vector tiles layer (by vector tiles layer ID) and each stylesheet (by semantic annotation ID)  

[#img_styles_sa,reftext='{figure-caption} {counter:figure-num}']
.Semantic Annotations for Styles and Vector Tiles
image::images/styles.png[]

==== Coupling Layers and Stylesheets Using Stylable Layer Sets
This approach uses semantic annotations to couple layers and stylesheets through stylable layer sets.
The following replaces <<coupling_with_semantic_annotations,the previous section>>.

. Ensure required tables are present:
** `gpkgext_relations`
** `gpkgext_semantic_annotations`
. Populate `gpkg_extensions` with:
** references to all tables mentioned above
** reference to the Related Tables Extension
. Add a row to `gpkgext_semantic_annotations` describing a URI for a stylable layer set
. Establish a related tables relationship between the vector tiles layers and semantic annotations.
.. Add a row to `gpkg_contents` for your user-defined mapping table.
.. Add a row to `gpkgext_relatons` for a related tables relationship between the user-defined vector tiles table and `gpkgext_semantic_annotations` via the user-defined mapping table
.. Add a row to the mapping table for each vector tiles layer (by vector tiles layer ID) and each stylable layer set (by semantic annotation ID)  
. Establish a related tables relationship between the stylesheets and semantic annotations.
.. Add a row to `gpkg_contents` for your user-defined mapping table.
.. Add a row to `gpkgext_relatons` for a related tables relationship between  `gpkgext_stylesheets` and `gpkgext_semantic_annotations` via the user-defined mapping table
.. Add a row to the mapping table for each stylesheet (by stylesheet ID) and each stylable layer set (by semantic annotation ID)  

[#img_stylable_layer_set,reftext='{figure-caption} {counter:figure-num}']
.Semantic Annotations for Stylable Layer Sets and Vector Tiles
image::images/stylable-layer-set-vt-only.png[]

