### Vector Tiles In GeoPackage
It is straght-forward to add vector tiles to a GeoPackage.

[#img_attributes,reftext='{figure-caption} {counter:figure-num}']
.Vector Tiles in GeoPackage
image::../images/vt.png[]

link:vtp.adoc[next]
