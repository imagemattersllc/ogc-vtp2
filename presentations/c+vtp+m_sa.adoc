### Conventional and VT Layers Coupled With Semantic Annotations
Semantic annotations provide a more flexible way to annotate a large variety of business objects. 

[#img_attributes,reftext='{figure-caption} {counter:figure-num}']
.Conventional and vector tiles layers with semantic annotations
image::../images/c+vtp+m_sa.png[]

The new design for semantic annotations flattens the table structure and reduces the number of tables needed.

The annotations that can be made include, but are not limited to, the following:

styles:: The URI in `gpkgext_semantic_annotations` (the same as in `gpkgext_styles`) allows conventional layers or vector tiles layers to be annotated as suitable for use with that style.
stylable layer sets:: A new URI allows conventional layers, vector tiles layers, and styles to be annotated as belonging to the same stylable layer set

#### Worked Example
The following tables are used as the basis for worked examples in subsequent subsections.

.gpkg_contents
[width="90%",options="header"]
|===
|rowid | table_name |data_type
|0 |basemap | tiles
|1 |tiles_daraa |vector-tiles
|2 |overlays   |features
|===

.gpkgext_styles
[width="90%",options="header"]
|===
|id | style | description | uri
|3  | night | Night style for OSMTDS| 'http://geosolutions.com/styles/osmtds-night/'
|4   | topographic   |Topographic style for OSMTDS| 'http://geosolutions.com/styles/osmtds-topographic/'
|===

.gpkgext_vt_layers
[width="90%",options="header"]
|===
|id | table_name | name
|13  | tiles_daraa | agricultureSrf
|14   | tiles_daraa | settlementSrf
|===

##### Styles
Let's say that by some bit of magic our styles work for both our vector tiles and our overlays.
We create two semantic annotations (one for each style).
We then tag our layers (both conventional and vector tiles) with those annotations.
Since it is plausible that individual layers in a vector tile set will use different styles,
the rows in `gpkgext_sa_reference` point to rows in `gpkgext_vt_layers` for vector tiles.

[NOTE]
====
The fact that the styles are also stored in the GeoPackage is secondary. 
It is not mandatory to do so, but when the architecture calls for it, 
the styles and semantic annotations will have the same URI.
====

.gpkgext_semantic_annotations
[width="90%",options="header"]
|===
|id | type | title | description | uri
|23  | style | night | Night style for OSMTDS| 'http://geosolutions.com/styles/osmtds-night/'
|24   | style | topographic   |Topographic style for OSMTDS| 'http://geosolutions.com/styles/osmtds-topographic/'
|===

.gpkgext_sa_reference
[width="90%",options="header"]
|===
|table_name | key_column_name | key_value | sa_id
|gpkg_contents | rowid | 2 | 23
|gpkgext_vt_layers | id | 13 | 23
|gpkgext_vt_layers | id | 14 | 23
|gpkg_contents | rowid | 2 | 24
|gpkgext_vt_layers | id | 13 | 24
|gpkgext_vt_layers | id | 14 | 24
|gpkgext_styles   | id | 3 | 23
|gpkgext_styles   | id | 4 | 24
|===

##### Stylable Layer Sets
Let's say that by some bit of magic we have a stylable layer set that works for both our vector tiles and our overlays.
Here we establish a stylable layer set as a semantic annotation. 
Our layers and styles all are annotated with this stylable layer set.
Since it is plausible that not all layers in a vector tile set belong to the same stylable layer set,
the rows in `gpkgext_sa_reference` point to rows in `gpkgext_vt_layers` for vector tiles.

.gpkgext_semantic_annotations
[width="90%",options="header"]
|===
|id | type | title | description | uri
|33  | StylableLayerSet | OSMTDS | stylable layer set for OpenStreetMap TDS | 'http://opengis/stylableLayerSets/OSMTDS'
|===

.gpkgext_sa_reference
[width="90%",options="header"]
|===
|table_name | key_column_name | key_value | sa_id
|gpkgext_vt_layers | id | 13 | 33
|gpkgext_vt_layers | id | 14 | 33
|gpkg_contents | rowid | 2 | 33
|gpkgext_styles | id | 3 | 33
|gpkgext_styles | id | 4 | 33
|===

link:c+vtp+m_simple_join.adoc[previous]
